package com.example.gitlab01.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String sayHello() {
        return "Hello this is my first api i have deployed online. i used git lab CICD for that";
    }

    @GetMapping("/")
    public String sayWelcom() {
        return "Welcome to my buitiful web site";
    }
}
