package com.example.gitlab01.controller;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class HelloControllerTest {

    @InjectMocks
    private HelloController helloController;

    @Test
    void test_say_hello() {
        // given

        // when
        String result = helloController.sayHello();

        // then
        Assertions.assertThat(result).isEqualTo("Hello BALDE");
    }

}