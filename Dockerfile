# Utilisez une image Maven pour construire le projet
FROM maven:3.9.6-eclipse-temurin-17 AS builder

# Définissez le répertoire de travail dans le conteneur
WORKDIR /app

# Copiez seulement le fichier pom.xml
COPY pom.xml .

# Téléchargez les dépendances Maven
RUN mvn dependency:go-offline -B

# Copiez le reste du code source
COPY src src

# Compilez l'application
RUN mvn package -DskipTests

# Utilisez une image OpenJDK pour exécuter l'application
FROM openjdk:17

# Définissez le répertoire de travail dans le conteneur
WORKDIR /app

# Copiez uniquement le fichier JAR généré dans la première étape
COPY --from=builder /app/target/gitLab01-0.0.1-SNAPSHOT.jar .

# Exposez le port sur lequel l'application s'exécute
EXPOSE 8080

# Commande pour exécuter l'application lorsqu'un conteneur est lancé
CMD ["java", "-jar", "gitLab01-0.0.1-SNAPSHOT.jar"]
